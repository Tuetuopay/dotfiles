#!/bin/bash

function prompt_install() {
	read -p "Install the $1 config? (Y/n) " answer
	[[ -z $answer || $answer =~ ^y(es)?$ ]]
}

function copy() {
	echo cp -rf $@
	cp -rf $@
}

# Install config for software with a single config file
# args: <program> <source> <destination>
function do_install {
	if prompt_install $1; then
		# Create target folder
		mkdir -p $(dirname $3)
		copy $2 $3
	fi
}

# Install config for software using XDG config dir. Write to XDG_CONFIG_HOME.
function do_install_xdg {
	if prompt_install $1; then
		[[ $(which $1) == "" ]] && echo "$1 is not installed. Installing anyway."
		copy .config/$1 ${XDG_CONFIG_HOME:-~/.config}/$1
	fi
}

# bashrc
if [[ $(uname -a | grep deb\[0-9\]+u) != "" ]]; then # Debian
	do_install bash .bashrc.debian ~/.bashrc
elif [[ $(uname -a | grep \.el\[0-9\]+\.) != "" ]]; then # CentOS / RedHat
	do_install bash .bashrc.centos ~/.bashrc
fi

do_install tmux .tmux.conf ~/.tmux.conf
do_install clang-format .clang-format ~/.clang-format

# vim config
if prompt_install vim; then
	copy .vimrc ~/
	copy .vim ~/
fi

for pgm in bat fish kitty sway swaylock waybar; do
	do_install_xdg $pgm
done

echo "All done!"
